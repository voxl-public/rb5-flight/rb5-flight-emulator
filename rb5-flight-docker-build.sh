#!/bin/bash

echo "[INFO] Building rb5-flight-docker"

IMAGE_NAME="rb5-flight-emulator"
IMAGE_TAG="1.0.0"

if ! [[ -d data/rootfs ]]; then
    echo "[ERROR] Missing rootfs.  See README for instructions"
    exit 1
fi

docker build \
--platform linux/arm64/v8 \
--tag "${IMAGE_NAME}:${IMAGE_TAG}" .

echo "[INFO] Done"
