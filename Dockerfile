# See https://github.com/tianon/docker-brew-ubuntu-core/blob/f6c798bd7248f69db272e17677153fc119f41301/bionic/Dockerfile

# 'scratch' is an empty docker image
FROM scratch

# add the rootfs contents from the system image build output
ADD data/rootfs /

# required for ARM emulation
COPY ./bin/qemu-arm-static /usr/bin/qemu-arm-static

# install helpers
RUN apt-get -y update
RUN apt-get -y install -y git cmake sudo

# start in /home/usr
WORKDIR /home/user

CMD ["/bin/sh"]
