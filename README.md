# rb5-flight-emulator

## Summary

The `rb5-flight-emulator` is an armv8 Ubuntu 18 Docker image that runs on your Linux PC to emulate the rb5-flight.

This project takes the rootfs output of the system image build (download below) and builds it on top of an armv8 Docker 'scratch' image. This provides developers an off-target workflow for developing for the rb5-flight.

For example, we use this image in our build servers for CI.

## Requirements 

- Ubuntu 18.04 host (other versions may work, but have not been tested)
- Docker
- QEMU, to install:

```bash
# install packages
sudo apt-get install qemu binfmt-support qemu-user-static

# Execute the registering scripts, see https://www.stereolabs.com/docs/docker/building-arm-container-on-x86/
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

## Usage

- Download the `1.0.0-M0052-9.1-perf-rootfs.tar.gz` image from <https://developer.modalai.com/>
- Copy the file into the `data` directory, for example, assuming the file was downloaded to `Downloads`:
  

```bash
cd ~/rb5-flight-emulator
cp ~/Downloads/1.0.0-M0052-9.1-perf-rootfs.tar.gz data
```

- Unzip into a new `rootfs` directory:

```
cd ~/rb5-flight-emulator/data
mkdir rootfs
tar -xzvf 1.0.0-M0052-9.1-perf-rootfs.tar.gz -C rootfs
```

The version (e.g. `1.0.0`) should match the `IMAGE_TAG` variable in the `rb5-flight-docker-build.sh` script.

Now, build the image by running the following:

```bash
./rb5-flight-docker-build.sh
```

*Note:* if you get failures, try running this again:

```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

To run from anywhere, install a helper script to `/usr/local/bin` using:

```bash
./rb5-flight-docker-install.sh
```

Reload bash:

```bash
bash
```

Run the docker image:

```bash
rb5-flight-docker
```

```bash
sh-4.4#
```

You can take look for the version here:

```bash
sh-4.4# more /etc/version 
1.0.0-M0052-9.1-perf
```
