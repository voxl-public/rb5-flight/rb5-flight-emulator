#!/bin/bash

echo "[INFO] Installing into /usr/local/bin"

sudo cp tools/rb5-flight-docker /usr/local/bin/
sudo chmod +x /usr/local/bin/rb5-flight-docker

echo "[INFO] Done"
